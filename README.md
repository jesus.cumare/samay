# Prueba Tecnica AWS 2 Samay Health

Desarrollo de prueba tecnica de Samay health

## Bridgetown Website README

Welcome to your new Bridgetown website! You can update this README file to provide additional context and setup information for yourself or other contributors.

## Prerequisites

Hay que modificar los parámetro o variables en cada archivo.


 - YOUR_ACCOUNT_ID 
 - YourCodePipelineRole
 - YourSageMakerRole 
 - YourCloudFormationRole 
 - YourCodeBuildRole 
 - YourCodeCommitRepository
 - YourSageMakerModel
 - your-sagemaker-container-image
 - your-s3-bucket
  
  y otros deben ser reemplazados con valores específicos en el entorno y configuración que se tenga en la cuenta de aws.


## IMPORTANTE

# Pipeline de Datos con AWS Lambda, SageMaker y GitLab

Este repositorio contiene los scripts y archivos necesarios para implementar un pipeline de datos utilizando AWS Lambda, SageMaker y GitLab.

## Estructura del Repositorio

  - `lambda_function.py`: Código de la Lambda que genera el archivo CSV.
  - `cloudformation`: Carpeta que contiene plantillas de CloudFormation para configurar los recursos de AWS.
  - `lambda-stack.yaml`: Plantilla para crear la función Lambda y sus recursos asociados.
  - `sagemaker-stack.yaml`: Plantilla para configurar los recursos de SageMaker.
  - `codebuild-project.yaml`: Plantilla para definir el proyecto de CodeBuild.
  - Otros archivos de configuración y scripts necesarios.

## 1. Crear la Lambda que genere la data de los sensores IoT con mediciones aleatorias.


### 1.1 Configuración Inicial

1. Clona este repositorio en tu máquina local.
2. Navega a la carpeta `cloudformation` y ejecuta la plantilla `lambda-stack.yaml` con AWS CloudFormation para crear la función Lambda y sus recursos asociados.

### 1.2 Configuración en la Consola de AWS

3. Accede a la consola de AWS y navega a la función Lambda recién creada.
4. Configura los detalles de la función, como el nombre y la configuración del gatillo (por ejemplo, S3).
5. Sube el código de la Lambda (`lambda_function.py`) que esta comprimida en el archivo GeneraData.zip en la carpeta Samay.

## 2. Ejecutar la Lambda

6. Ejecuta la función Lambda manualmente o configura un evento en el bucket de S3 para desencadenar la ejecución automáticamente.

Nota: Modificar el parametro s3_bucket en el codigo de la lambda

## 3. Crear y Analizar el Último Archivo CSV con SageMaker


### 3.1 Configuración de los Recursos de SageMaker

7. Ejecuta las plantillas de CloudFormation `sagemaker-stack.yaml` y `codebuild-project.yaml` para crear los recursos de SageMaker y el proyecto de CodeBuild.

### 3.2 Ejecución del Pipeline en SageMaker

8. Inicia el pipeline en SageMaker, ya sea a través de la consola o mediante el comando AWS CLI.
   ```bash
   aws sagemaker start-pipeline-execution --pipeline-name YourSageMakerPipeline

Este repositorio contiene los archivos y scripts necesarios para implementar un proceso seguro y cifrado en AWS utilizando servicios como CodeCommit, CodeBuild y SageMaker.

## Estructura del Repositorio

- `main.yaml`: Plantilla principal de CloudFormation que define el pipeline de CI/CD.
- `sagemaker-stack.yaml`: Plantilla de CloudFormation para configurar recursos de SageMaker.
- `codebuild-project.yaml`: Plantilla de CloudFormation para definir el proyecto de CodeBuild.
- `buildspec.yml`: Archivo de configuración para CodeBuild.
- Otros scripts y archivos necesarios para el proceso.

## Pasos para Implementar

1. **Configuración del Repositorio en CodeCommit:**
   - Crea un repositorio en CodeCommit y clona este repositorio en local.
   - Sube tu código fuente y otros archivos al repositorio de CodeCommit.

2. **Configuración del Pipeline en CodePipeline:**
   - Ejecuta la plantilla `main.yaml` en CloudFormation para crear el pipeline de CodePipeline.
   - Configura el pipeline para que use el repositorio de CodeCommit como origen.

3. **Configuración del Proyecto de CodeBuild:**
   - Ejecuta la plantilla `codebuild-project.yaml` en CloudFormation para crear el proyecto de CodeBuild.
   - Ajusta las configuraciones según sea necesario, asegurándote de utilizar prácticas de seguridad.

4. **Configuración de SageMaker:**
   - Ejecuta la plantilla `sagemaker-stack.yaml` en CloudFormation para configurar los recursos de SageMaker.
   - Personaliza las configuraciones según tus necesidades, especialmente en términos de seguridad y cifrado.

5. **Ejecución del Pipeline:**
   - Desencadena manualmente o configura eventos de CodePipeline para que el pipeline se ejecute automáticamente.
   - Monitorea el progreso y los registros en la consola de AWS.

## Consideraciones de Seguridad y Cifrado

- Asegúrate de tener políticas de IAM bien definidas y configuradas con el principio de menor privilegio.
- Implementa cifrado en repositorios, conexiones y datos almacenados utilizando las opciones proporcionadas por AWS.
- Realiza análisis de seguridad en tu código fuente y en los modelos de SageMaker.



6. **Para crear una canalización (pipeline) en SageMaker que analice el archivo CSV generado con la Lambda:**

Vamos a llamarlo sagemaker-pipeline-definition.json. Este archivo debe contener las etapas y acciones necesarias para que funcione el pipeline.

Creación del Pipeline en SageMaker:

Ejecuta el siguiente comando de AWS CLI para crear el pipeline en SageMaker:

```sh
bash
Copy code
aws sagemaker create-pipeline --pipeline-name YourSageMakerPipeline --pipeline-definition file://sagemaker-pipeline-definition.json --role-arn arn:aws:iam::YOUR_ACCOUNT_ID:role/YourSageMakerRole
```

Asegúrate de reemplazar las rutas de S3, URIs de imágenes, nombres de roles y otros valores con los apropiados para tu configuración.

Ejecución del Pipeline:

Puedes iniciar la ejecución del pipeline a través de la consola de SageMaker o mediante el siguiente comando de AWS CLI:

```sh
bash
Copy code
aws sagemaker start-pipeline-execution --pipeline-name YourSageMakerPipeline

```
Monitorea el estado del pipeline y verifica los registros en la consola de SageMaker para asegurarte de que cada etapa se ejecute correctamente.


## Contribuciones y Problemas

- Es posible que me falten pasos mas detallados jejeje.

- Queria mostrarlo y publicar el empoint para que mostrara los modelos y la ejecucion del pipeline
pero esta infraestructura es costosa solo haciendo las pruebas deje encendido el ambiente y se me fuero algunos dolaritos.

## Imagenes de los componentes configurados del proyecto

- Artifacto

 ![parte delantera](Artifactos.PNG)
 
- CLoudWatch

 ![parte delantera](CLoudWatch.PNG)
 
- codeBuild

 ![parte delantera](codeBuild.PNG)
 
- CodePipeline

 ![parte delantera](CodePipeline.PNG)
 
- Experimentos

 ![parte delantera](Experimentos.PNG)
 
- IAM

 ![parte delantera](IAm.PNG)
 
- Lambda ejecucion genera csv

 ![parte delantera](LambdaEjecucionGenera_csv.PNG)
 
- Pipeline

 ![parte delantera](pipeline.PNG)
 
- pipeline1

 ![parte delantera](pipeline1.PNG)
 
- Proyecto ON

 ![parte delantera](proyecto.PNG)

 - QuickSight

 ![parte delantera](QuickSight.PNG)

 - S3

 ![parte delantera](Samay/Imagenes/S3.PNG)